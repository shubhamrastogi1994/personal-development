
The Business School For People who like helping People


Introduction: Why Do I Recommend Network Marketing As A Business?

	- If you are thinking about starting a network marketing business, 
	- I believe you will find out about some of the hidden opportunities and values a network marketing business can offer you, 
	values that many people often fail to see. 
	- In other words, there is far more to a network marketing business than just the chance to make some extra money.

Chapter 1: What Makes The Rich, Rich?

	- It is a “Business School,” the business school for real life.
	- “If you want to be rich, you need to be a business owner and an investor.”
	- Why Thomas Edison Was Rich And Famous
		- newspaper owned and published
		- telegraph operator.
	- “Being a telegraph operator he knew that what made the inventor of the telegraph so successful was 
		because it was a business system, a system of lines, poles, skilled people, and relay stations.
		Thomas Edison understood the power of a system.”

	- The system was more important than the invention.”
	- most people work for the system rather than own the system,”
	- “And he could see the big picture because of his business experience on the train and his experience as a telegraph operator,”
	- The richest people in the world build networks. Everyone else is trained to look for work.
	- telephone network AT&T is also a network
	- most people are just happy to find a job working as an employee of a large network… a network that makes the rich, richer.
	- A well designed and managed business network will exponentially earn far more than a hard-working individual.
	- “If you want to become rich, you need to network with those who are rich or who can help you become rich.”
	- A Business School For The People
	- A big reason I support the network marketing industry is that many companies in the industry are really business schools for the people,
	  rather than business schools that take smart kids and train them to be employees of the rich.


Chapter 2: There Is More Than One Way To Become Rich
	
	- “Because I want to be free. I want to have the money and time to do what I want to do. 
	   I don’t want to be an employee for most of my life. I don’t want my life’s dreams to be dictated by the size of my paycheck.”
	- the three different types of education that are required if we want to be financially successful in life: 
		scholastic, professional and financial education.
	- Scholastic Education
		- This education teaches us to read, write and do math. It is a very important education, especially in today’s world.
	- Professional Education
		- This education teaches you how to work for money.
		- Other professional schools teach people to become medical assistants, plumbers, builders, electricians and automobile mechanics.
	- Financial Education
		- This is the education where you learn to have money work for you rather than you work for money. 
		  This third level of education is not taught in most of our schools.
		- “If you have a poor financial education, you will always work for the rich.”
	- “Learn to have money work for you rather than you work for money.”
	- A Financial Disaster
		- “Your living expenses go down after you retire.”
	- Learning how to manage money and invest money is certainly as important as learning how to dissect a frog.
	- “Building your own business is the best way to become rich. 
	    After you have built your business, and you have strong cash flow, then you can begin investing in other assets.”
	- The Other Ways To Become Rich
		1. You can become rich by marrying someone for his or her money.
		2. You can become rich by being a crook.
		3. You can become rich by being greedy.
			- greed, corruption and lack of moral guidance, proving that not all crooks deal in drugs, wear masks and rob banks.
		4. You can become rich by being cheap.
			- live below their means, rather than finding ways to expand their means.”
			- in the end you’re still cheap.”
		5. You can become rich via hard work.
			- Working Hard For A Bad Income the highest taxed of all income.
		6. You can become rich by being exceptionally smart, talented, attractive or gifted.
		7. You can become rich by being lucky.
			- lottery winners
		8. You can become rich by inheriting money.
		9. You can become rich by investing.
		10. You can become rich by building a business.
			- franchises
			- The Difference Between Big Business Owners And Small Business Owners
				big business people build networks.
				small business owners who own restaurants.

	- The 11th Way To Become Rich . You can build a network marketing business.
		- “A network marketing business is a new and revolutionary way to achieve wealth.”
		- personal franchise or an invisible big business network,
		- “A network marketing system, a system I often call a personal franchise, is a very democratic way of wealth creation.”
		- Henry Ford, “Democratize the automobile.”
		- because he cared not only for his customers but because he cared for his workers. He was a generous man rather than a greedy man.
		- “Thinking is the hardest work there is. That is why so few people engage in it.”
	
	- Wealth For Everyone
		- While this new form of a business system is not for everyone, if you are a person 
			who truly wants to help as many people as possible achieve their financial goals and dreams, 
			then the network marketing business is worthy of your time to look into it.

Chapter 3: Value #1: True Equal Opportunity
	
	- The Closing Of My Mind
	- A Change Of Mind
		- “The problem is, most cannot invest with me because they do not have enough money.
		- You and I know the best real estate deals go to the rich who have money and not to those without money.”
		- Slowly I began to understand his reasons for being in the business. His primary reasons were:
			1. He wanted to help people.
			2. He wanted to help himself.
			3. He loves learning and teaching.
		- It’s a great business and a great real-life business school.”

	- A Newly Opened Mind
		- “A network marketing business has an open door policy.”
		- If you are willing to stick it out, and learn and study at your own pace, 
		  the business will continue to stick by your side. 
			Many network marketing companies are truly equal opportunity businesses. 
			If you will invest the time and effort, so will they.
	- “If you give a poor person money, you only keep them poor longer.”


Chapter 4: Value #2: Life-Changing Business Education
	
	- It’s Not About The Money
	- It’s Not About The Products
	- It’s The Education Plan
		- network marketing companies do have excellent education and training plans…
	- What To Look For In An Education Plan
		- “turning caterpillars into butterflies.”
		- Therefore, when I speak of life-changing business education, 
			I speak of education powerful enough to change a caterpillar into a butterfly, a process often called a metamorphosis. 
		- When you look into the educational plan of a network marketing company, 
		  choose that has the power to make that much of a difference in your life.
	- Real Life Business School
		- Rich dad’s business school was a business school that focused on the skills 
		 that made a person rich rather than the theories of what made a businesses and the economy run. 
		- “Skills make you rich, not theories.”
	- However, more important than teaching just real world business skills, 
		the leaders taught real world mental and emotional attitudes that are required to be successful in the world. 
	- The education I found in some of the seminars was, priceless… absolutely priceless, especially for anyone who wants to transform into a butterfly.
	- One reason for their passion is because a network marketing business is based upon the leaders pulling people up,

	- What Is Life-Changing Education?
		- Mental Education
			- Traditional education focuses primarily on mental education.
		- Emotional Education
			- “If you don’t get good grades, you won’t get a high-paying job.”
			- The problem is, in the real world, the people who get ahead are the people who make the most mistakes and learn from them.
			- “Making mistakes is how we are designed to learn.
			- That is why so many people in the corporate world remain caterpillars.
			- In network marketing, you are encouraged to make mistakes, correct, learn and grow.
			- To me that is life-changing education. When you let go of your fear you can begin to fly.
			- If you are a person who is terrified of making mistakes and afraid of failing, 
				then I believe a network marketing business with a great educational program is especially good for you. 
				I have witnessed network marketing training programs that build and restore a person’s self-confidence… 
				and once you have more confidence, your life is changed forever.
		- Physical Education
			- The network marketing companies I have studied all encourage physical learning as much as they encourage mental learning. 
			- They encourage you to go out and face your fears by taking action, making a mistake, learning from the mistake, 
				and growing mentally, emotionally and physically stronger from the process.
			- Traditional education encourages you to learn the facts and then emotionally teaches you to be afraid of making mistakes, 
				which holds you back physically.
			- Living in an environment of fear is not healthy, mentally, emotionally, physically or financially.
			- You cannot improve your future if you are not willing to try something new and risk making mistakes and learning from them.
			- It has been said that if you want to change what a person does, change the way they think. 
				Lately, more people are subscribing to the idea that if you want to change the way a person thinks, 
				first change what they are doing. The great thing about a network marketing business is that it focuses on both what you think and what you do.
		- Spiritual Education
			- Beyond Human Limitations
			- When I speak of a person’s spirit, I speak of the power that propels us past our mental, emotional and physical limitations… 
			  limitations that often define our human condition.
			- Leaders ignited their spirits to go on and defeat the most powerful army in the world.
			- successful leaders in network marketing have been trained to develop this ability to speak to the human spirit. 
				They have the ability to touch the greatness in those coming behind them and to inspire them to move up… 
				to go beyond their human limitations, go beyond their doubts and fears. 
				That is the power of life-changing education.
	- “There are three people in all of us. There is a rich person, a poor person, and a middle class person. It is your job to find out which ones come out.”
	- The word education means to draw out. One of the problems I have with traditional education was that it is based on fear, 
		the fear of failing, rather than the challenges of learning, learning from our mistakes. 
		In my opinion, traditional education is designed to draw out the middle class person in us… 
		the person that feels insecure, needs a job, a steady paycheck, lives in fear of making mistakes, 
		and worries about what their friends might think of them if they do something different.


Chapter 5: Value #3: Friends Who Will Pull You Up, Not Push You Down
	
	- The Hardest Job Of All
	- the hardest part of leaving a secure job and starting a business was dealing with what my friends, 
		family and co-workers would say or think. For me, that was the hardest job of all.
	- A Change Of Quadrants, Not A Change Of Jobs
	- What Is The CASHFLOW Quadrant?
		- E quadrant Values
			- “I am looking for a safe, secure job with benefits.” Or, “How much do we get for overtime?” 
				Or, “How many paid holidays do we have?” 
				In other words, security is a very important core value for someone in the E quadrant.

		- S Quadrant Values
			- independence.
			- The way an S gets paid is often by commission or by the amount of time they spend on a job.
		
		- B Quadrant Values
			- powerful life missions, value a great team and efficient team work, and want to serve and work with as many people as possible.
			- “If I stop working today how much income continues to come in?” 
				If your income stops in six months or less, then chances are, you are in the E or S quadrants. 
				A person in the B or I quadrant can stop working for years and the money will continue to come in.
		
		- I Quadrant Values
			- financial freedom.

	- Different Quadrants, Different Investors
		- “Just because you’re successful in one quadrant, such as the E, S, or B quadrant does not mean you will be successful in the I quadrant.”

	- A Network Marketing Business Is A B Quadrant Business
		- A network marketing business is for people who want to enter the world of the B quadrant. 
		- Why is it a B quadrant business? 
			The answer is because the network marketing business system is designed to expand to well over  people. 
			Also, the income potential in a network marketing business is, in theory, unlimited, 
			while the income potential for the E and S quadrants are often limited, limited to how much you as an individual can produce. 
			In a network marketing business, you can earn as much as your network can earn. 
			If you build a big network, you can earn a massive amount of money.
		- What Do You Want To Be When You Grow Up?
		- “In which quadrant do I have the most chance of financial success?”
	- A Change Of Quadrants Means A Change Of Values And Friends
		- At least a network marketing business provides a large support group of like-minded people, 
			people with the same core values, the values of the B quadrant, to assist you while you make your transition. 
		- All I had was my rich dad and his son to encourage me.
	- Which Quadrant Are Your Friends In?
		- Yet my core friends are in the B and I quadrants.
		- “Changing from one quadrant to another is a change of core values.”
	- One of the reasons I believe network marketing is so difficult to explain is simply 
		because there are very few people who are successful in the B quadrant. 
		Most people, due to our schools and family values, are in either the E or S quadrants. 
	- In fact, I would estimate that 80% of the population is in the E or S quadrant. 
	  I would also say 15% are in the I quadrant and that less than 5% of the population are genuinely in the B quadrant.
	- 5-Year Plan
		- Most people want money but are unwilling to invest their time.
		- learning is a physical process… and physical learning sometimes takes longer than just mental learning.
		- unlearning is also a physical process. There is a statement that goes, “You can’t teach an old dog new tricks.”
		- All caterpillars make a cocoon before becoming butterflies.
		- My process of becoming a businessperson and investor has followed much the same process as becoming a pilot ready to go into battle. 
		 It took my failing twice in business before I suddenly found my spirit… a spirit often called the “entrepreneurial spirit.” 
	- I Still Use The 5-Year Plan
		- When I decide to learn something new, for example, investing in real estate, I still allow myself five years to learn the process.
	- My Journey In The B And I Quadrants Has Not Ended
	- The good thing is the more I learn, the more I earn and the less I have to work.
	- I would recommend committing to a minimum of five years of learning, growing, changing your core values and meeting new friends. 
	- To me, those changes are far more important than a few extra bucks.


Chapter 6: Value #4: What Is The Value Of A Network?

	- telecopier…
	- A Network’s Economic Value = Number of Users2
	- “The economic value of a network goes up exponentially, not arithmetically.”
	- A Franchise Is A Network
	- The Second Type Of Networked Business
		- network of franchised individuals.
	- One of the reasons most people cannot see the rapid growth of network marketing is simply 
		because network marketing is in most cases an invisible business. 
	- Unlike signs that shout McDonalds or Starbucks, most network marketing franchises operate discreetly out of homes or small offices. 
	- On top of that, many successful network-marketing franchises make far more money than most franchises.
	- Big Business Is Now In The Network Marketing Business
		- The reason the industry continues to grow is due to the power found in Metcalf’s Law.
	- Harness The Power Of Metcalf’s Law
		- “Your job is to clone or duplicate someone just like you.”
	- I talked about the value of new friends. If you will invest a little time and explain to them first the CASHFLOW Quadrant, 
		asking them which quadrant they want to invest their time in,
	- then explain to them the power of Metcalf’s Law, I believe you will have a person who is far more receptive to the business opportunity you are presenting.
	- Building a network marketing business is simply looking for friends, new and old, who want to go in the same direction you want to go.
	- A Better Idea That Costs Less To Get Into
	- The network marketing business is exploding worldwide very few people can see it.
	- The Future Of Networking
	- Today people need new ideas and systems via which they can find the financial security our parents once had.
	- One answer is network marketing.
		- Network marketing gives millions of people throughout the world the opportunity to take control of their lives and their financial future. 
		- That is why the network marketing industry will continue to grow, even though old world thinkers fail to see it growing.
	- All you have to do is say, “I want the power of networks working for me.”
	- The beauty of network marketing is that it teaches you this very valuable skill, a skill you can use to increase your wealth for the rest of your life.

Chapter 7: Value #5: Developing Your Most Important Business Skill

	- “Go get a job in sales.”
	- “The ability to sell is the number one skill in business.”
	- “The ability to sell is the most important skill of the B quadrant. If you cannot sell, don’t bother thinking about becoming a business owner.”
	- The best salespeople are the best leaders,”
	- All great teachers have been great salespeople.
	- We are all born sales people.”
	- Buy / Sell
		- If You Want To Buy You Have To Sell Something First
	- “Poor people are poor because they cannot sell… or they have nothing to sell.
	- “You mean when I ask a girl out for a date, you consider that selling?”
	- “So selling affects every aspect of life,”
	- “If you want to be successful in life, you need to learn how to sell,”
	- “People learn to overcome their fears rather than let their fears run their life.”
	- My Sales Education Begins
	- Bringing Out The Winner In You
	- “There is a rich person and a poor person inside each of us. There is also a winner and a loser inside of each of us. 
		Every time we let our fears, our doubts or our low self-esteem win, the loser wins. 
		Learning to sell is learning to overcome the loser inside of you. 
		Learning to sell also brings out the winner in you.”
	- One of the beauties of network marketing is that it gives you the opportunity to face your fears, 
		deal with your fears, overcome your fears, and let the winner in you win. 
	- The beauty of network marketing companies is that the leaders in the organization have the patience to work with you while you are learning.
	- Sales Training Helped Me Meet The Woman Of My Dreams
	- A Word On Rejection
		Rejection = Success
	- “The most successful people in the world are the most rejected people in the world.”
		Rejection and Correction = Education and Acceleration
	- “Rejection is the start of education.”
	- “The more I risk being rejected, the better my chances are of being accepted.”
	- The lesson I learned was that the more I risk being rejected, the better my chances are of being accepted.
	- 98% Rejection
		- The lesson for you is this: if you want to become more successful in life, simply seek more rejection and then correction. 
		The beauty of a network marketing business is that the leaders encourage you to go out and get some rejection.

	- Teaching Versus Selling
		- In network marketing not only do you have to learn to sell you also have to learn to teach others to sell.
		- If you can sell, but you cannot teach others to sell, you will not be successful in network marketing. 
		- That means the great thing about network marketing is that if you are going to be successful, you have to be a great teacher.
	- Sales Managers Don’t Sell, They Teach
	- Sales Dogs	
		- “The reason sales training is so important in a network marketing business is because not only do you have to learn how to sell, 
		 you also have to learn how to teach others to sell. If you do not teach others to sell, you will not be successful in a network marketing business.”
	- Credit Card Debt
		- The reason most people are in credit card debt is that they have been taught to be great buyers, but not to be great sellers.
		- To me that makes more sense than selling your tomorrows. You and I know that there is not much of a future when you sell your tomorrows.
	- “In the B quadrant leadership skills are not optional.”


Chapter 8: Value #6: Leadership
	
	- “Leaders do what most people are afraid of doing.”
	- One of great values of a network business is that it helps build that kind of leadership skill in their leaders.
	- Leadership Skills Are Not Optional
	- In the world of business, it was security, not freedom, that motivated people, money not mission, individual not team, and management not leadership.
	- Managers Are Not Leaders
	- Those leadership skills are very different from the management skills most often required for the E and S quadrants.
	- “Managers are not necessarily leaders and leaders are not necessarily managers.”
	- a friend of a friend came to me because he wanted to raise some money to start his own restaurant.
		- Although he had experience, charm, and charisma, he lacked the leadership skills to inspire confidence.
		- When you look at the CASHFLOW Quadrant, the difference between the S and B quadrants is size.
		- The third reason I did not invest is that if he was to remain small, then why should I invest?
		- The fourth reason for not investing with him was because he had to be the smartest member on his team. He had an ego problem.
	- In a B quadrant business, leadership skills are important simply 
		because the B person has to deal with people who are much smarter, more experienced, and more capable than he is or she is.
	- “A” Students Work For “C” Students
		- you have great leadership skills,
		- “In growing a business, leadership skills are a necessity.”
		- money, confidence and leadership, go hand in hand,
		- leadership skills are not optional in the B quadrant.
	- The Best Training In The World
		- one of the most important values I found in some network marketing businesses was their life-changing business education.
		- also found some of the best business and leadership development programs in the world.
	- I have met many successful entrepreneurs who received their business education in a network marketing business.
	- Leaders Speak To Your Spirit
		- The reason it takes leadership is because many people use the same types of words, words about dreams, more time with family, freedom, 
		   but few people are inspired enough to trust and follow the speaker of these over-used words.
	- Killing Your Spirit
	- Life changing education affects you mentally, emotionally, spiritually and physically.
	- Communicating Emotion-To-Emotion
		- In my opinion, too much of communication today uses fear or greed to motivate people into doing something. 
		- When the emotions of fear and greed are the primary motivators, it kills our spirits.
	- True Leaders Inspire The Spirit
	- Communicating Spirit-To-Spirit
		- “My best friend is the one who brings out the best in me.”
	- In the network marketing world, the type of leader that is developed is a leader 
		that influences others by being a great teacher, teaching others to fulfill their life’s dreams 
		by teaching others to go for their dreams.
	- In summary, all three types of leadership speak to the human spirit, but the different leadership styles bring out a different type of leader.
	- difference in value between money and wealth.
		- One of the reasons many people are not successful in a network marketing business is 
			because they come to the business looking for money, 
			rather than looking for an opportunity to build wealth.
	- “The rich don’t work for money. The poor and middle class do that.”


Chapter 9: Value #7: Not Working For Money
		
	- Yet the reason Kim and I were able to become financially free in fewer than  years was simply 
		because we knew the difference between money and wealth.
	- Three Ways To Live
		1. The feeling of fear.
		2. The feeling of anger and frustration.
		3. The feeling of joy, peace and contentment.
	- the feeling of not having to work, to be able to quit anytime, and still have more than enough money coming in for as long as we live, is a great feeling.
	- The Difference Between Money And Wealth
		- suspect that if they did not change their core values, they would remain for the rest of their lives in Life #2, 
			living with anger and frustration all their lives simply because they chose to work for money rather than wealth.
	- What Is Wealth?
		- In previous books and other rich dad products, I mention that wealth is not measured in money, instead wealth is measured in time. Our
		- Wealth is the ability to survive so many number of days forward.
		- Health and wealth are similar because both are measured in time.
	- Network Marketing Teaches You To Work For Wealth
	- If you want to become wealthy, you need money that comes from assets.
	- the guests responded positively to that idea, I doubt most thought it possible and besides, 
		it seemed that most just wanted an extra $3, a month next month, rather than having to work for a few years for free, building a business, 
		and then later on having the money come in forever. 
	- It is my guess that most people there were still thinking like employees of self-employed people, people who work for money, 
		rather than business owners and investors, people who work for wealth from assets.
	- A Different Kind Of Money
		- “Your financial statement is your report card after you leave school. Your financial statement measures your financial IQ.”
	- A Difference In Focus
		- Are You Still Working For Money?
		- The people in the B and I quadrants focus on building or acquiring assets rather than working for money.
		- That is why, the wealth of a B or I person is higher than an E or S quadrant person is.
	- The Three Types Of Assets
	- Simple Plan To Financial Freedom
	- How Can I Afford To Buy Real Estate?
	- “Keep your day time job and start building a part-time business. Once the business is making money, 
		the second step is to keep your daytime job and begin to buy investment real estate with the extra-income from the business.
	- Three Types Of Intelligences
		- Mental Intelligence:
		- Emotional Intelligence: emotional intelligence is  times more powerful than mental intelligence.
			keeping ones cool rather than arguing back; not getting married to someone you know in the long run will not be a good life’s partner; 
			and delayed gratification, which is the opposite of instant gratification, the cause of many of today’s financial problems.
	- Financial Intelligence:
		- “Your financial intelligence is measured on your personal financial statement. 
			Your financial intelligence is measured by how much money you make, how much money you keep, 
			how hard that money works for you, and how many generations you can pass on the money.”
	- Why Smart People Fail To Become Rich
		- they have a high IQ 
		- lack the emotional intelligence required for financial success and wealth accumulation.
		- They get rich too slowly because they play the money game too cautiously.
		- Trying to get rich too quickly.
		- This person lacks the emotional intelligence known as patience.
		- They start something, get bored and quit.
		- Spends on impulse.
		- “Money seems to run through my fingers.”
		- Cannot stand owning anything of value.
	- Many of these same people, rather than build a business, feel better working for someone else 
		and working hard at something they will never own. 
		The emotion of fear is so high that they would rather work for security rather than work for freedom.
	- Emotional Intelligence Is Essential To Financial Intelligence
	- If you are out of control emotionally, your chances of solving your financial challenges are reduced.
	- Having a high emotional IQ is essential to having a high financial IQ.
	- “A person that cannot manage their emotions cannot manage their money.”
	- “Live for today.”
	- “Come back when you’ve grown up. Then I’ll get back to teaching you to be rich.”
	- Do You Want To Improve Your Emotional IQ?
	- I work on improving my emotional IQ if I wanted to improve my financial IQ.
	- The first step in increasing my emotional intelligence is to admit I need to improve it.

Chapter 10: Value #8: Living Your Dreams
	
	- Rekindling The Dream
	- It was about inspiring others to go for their dreams.
	- Killing The Dream
	- “How can I afford it?”
	- Why Dreams Are Important
	- “People who dream small dreams continue to live lives as small people.”
	- The size of the dream was what was important.
	- “Big people have big dreams and small people have small dreams. If you want to change who you are, begin by changing the size of your dream.”
	- “Broke is temporary and poor is eternal. Even if you are broke, it does not cost you anything to dream of being rich. 
		Many poor people are poor because they have given up on dreaming.”

	- Different Types Of Dreamers
		1. Dreamers who dream in the past.
		2. Dreamers who dream only small dreams.
		3. Dreamers who have achieved their dreams and have not set a new dream.
		4. Dreamers who dream big dreams but do not have a plan on how to achieve them… so they wind up achieving nothing.
			- People like this should keep dreaming big, find a plan, and find a team that will help them make their dreams come true.”
		5. Dreamers who dream big dreams, achieve those dreams and go on to dream bigger dreams.
	- One of the most refreshing things that happened to me while looking into some of the network marketing businesses was that I found myself dreaming even bigger dreams.
	- If you are a person with big dreams and are a person who would love to support others in achieving their big dreams, 
		then the network marketing business is definitely a business for you.

Appendices: Value #9: Marriage and Business
	
	- Working Together
	- In the network marketing industry, I see many couples building their businesses together. 
	- To me this is a perfect business for couples that want to go into business together for several reasons:
		- It’s a business you can both begin part-time
		- You dictate the hours to fit your schedules
		- The industry supports families in business together
		- Many of the most successful people in the industry are couples
		- The education many network marketing companies offer allows couples to learn and grow together
	
Appendices: Value #10: The Family Business

	- There is a low entry cost to get started in a network marketing business.
		- Many companies provide good education and training programs to help you become successful.
		- There are mentors, successful in the business, ready to assist you in your journey.
	- Family is the most important thing to me.
	- Create Your Own Family Business
	- True Wealth Is Measured In Time Not Money
	- Rich Dad defines wealth in time not money. The more successful you become, the more time you will have and 
		the more freedom you will have to spend with your family.
	- Congratulations on selecting a family focused business. May your family share the gifts of love and togetherness from your success!
	
Appendices: Value #11: How You Can Use The Same Tax Loopholes The Rich Use

	- Start A Part-Time Business
	- Operate in a businesslike manner. 
	- Commit your time, effort and intention to make this profitable. 
	- Have, or will have, a reliance on the income. 
	- Experience losses which are either normal or beyond your control (if you experience losses). 
	- Make changes in attempt to achieve profit. 
	- Possess (or your advisors possess) knowledge in this area. 
	- Have experience making profit in this type of business or you have reasonable expectation of profit from future appreciation of assets.
	- How To Discover Your Hidden Business Deductions
		1. Home Office 
		2. Computers and Software
		3. Travel 
		4. Children		
	- Where there is a will there is way
	
	
